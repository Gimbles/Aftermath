open System
open System.IO
open aftermath

let VERSION = "ver.00000"
let RED = "\u001b[31m"
let GREEN = "\u001b[32m"
let YELLOW = "\u001b[33m"
let RESET = "\u001b[0m"

let banner =
    $"{GREEN}aftermath {YELLOW}{VERSION}{RESET} (host: {YELLOW}{System.Runtime.InteropServices.RuntimeInformation.OSDescription}{RESET}) on {YELLOW}{System.Runtime.InteropServices.RuntimeInformation.OSArchitecture}{RESET}"

if Environment.GetEnvironmentVariable "AF_QUIET" <> "1" then
    Console.WriteLine(banner)

let argv = Environment.GetCommandLineArgs()

if argv.Length <> 3 then
    // Why len - 32: the original string contains ANSI color codes too, which are included in the length but are not visible
    Console.WriteLine(String.replicate (banner.Length - 32) "─")

    printfn
        $"{RED}error{RESET}   expected {GREEN}2{RESET} arguments, found {RED}{argv.Length - 1}{RESET}\n{YELLOW}usage{RESET}   aftermath {GREEN}<source>{RESET}           {GREEN}<destination>{RESET}\n{GREEN}example{RESET} aftermath {GREEN}./bin/test_x86.exe{RESET} {GREEN}./bin/test_arm.exe{RESET}"

    exit 1

let source = File.ReadAllBytes argv[1]
x86toarm.translate source |> ignore
let destination = File.OpenWrite argv[2]
