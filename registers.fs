namespace aftermath.registers

open AsmArm64
open type AsmArm64.Arm64Factory
open Iced.Intel

module arm =
    let reg64orsp (register: Register) : Arm64RegisterXOrSP =
        match register with
        | Register.RDI -> Arm64RegisterXOrSP.op_Implicit X0
        | Register.RSI -> Arm64RegisterXOrSP.op_Implicit X1
        | Register.RDX -> Arm64RegisterXOrSP.op_Implicit X2
        | Register.R10 -> Arm64RegisterXOrSP.op_Implicit X3
        | Register.R8 -> Arm64RegisterXOrSP.op_Implicit X4
        | Register.R9 -> Arm64RegisterXOrSP.op_Implicit X5
        | Register.RBX -> Arm64RegisterXOrSP.op_Implicit X6
        | Register.RCX -> Arm64RegisterXOrSP.op_Implicit X7
        | Register.RAX -> Arm64RegisterXOrSP.op_Implicit X8
        | Register.RBP -> Arm64RegisterXOrSP.op_Implicit X9
        | Register.R11 -> Arm64RegisterXOrSP.op_Implicit X10
        | Register.R12 -> Arm64RegisterXOrSP.op_Implicit X11
        | Register.R13 -> Arm64RegisterXOrSP.op_Implicit X12
        | Register.R14 -> Arm64RegisterXOrSP.op_Implicit X13
        | Register.R15 -> Arm64RegisterXOrSP.op_Implicit X14
        | Register.RSP -> Arm64RegisterXOrSP.op_Implicit SP
        | otherwise -> invalidArg "register" $"{otherwise} is not a 64 bit register"

    let reg64 (register: Register) : Arm64RegisterX =
        match register with
        | Register.RDI -> X0
        | Register.RSI -> X1
        | Register.RDX -> X2
        | Register.R10 -> X3
        | Register.R8 -> X4
        | Register.R9 -> X5
        | Register.RBX -> X6
        | Register.RCX -> X7
        | Register.RAX -> X8
        | Register.RBP -> X9
        | Register.R11 -> X10
        | Register.R12 -> X11
        | Register.R13 -> X12
        | Register.R14 -> X13
        | Register.R15 -> X14
        | otherwise -> invalidArg "register" $"{otherwise} is not a 64 bit register"

    let reg32orwsp (register: Register) : Arm64RegisterWOrWSP =
        match register with
        | Register.EDI -> Arm64RegisterWOrWSP.op_Implicit W0
        | Register.ESI -> Arm64RegisterWOrWSP.op_Implicit W1
        | Register.EDX -> Arm64RegisterWOrWSP.op_Implicit W2
        | Register.R10D -> Arm64RegisterWOrWSP.op_Implicit W3
        | Register.R8D -> Arm64RegisterWOrWSP.op_Implicit W4
        | Register.R9D -> Arm64RegisterWOrWSP.op_Implicit W5
        | Register.EBX -> Arm64RegisterWOrWSP.op_Implicit W6
        | Register.ECX -> Arm64RegisterWOrWSP.op_Implicit W7
        | Register.EAX -> Arm64RegisterWOrWSP.op_Implicit W8
        | Register.EBP -> Arm64RegisterWOrWSP.op_Implicit W9
        | Register.R11D -> Arm64RegisterWOrWSP.op_Implicit W10
        | Register.R12D -> Arm64RegisterWOrWSP.op_Implicit W11
        | Register.R13D -> Arm64RegisterWOrWSP.op_Implicit W12
        | Register.R14D -> Arm64RegisterWOrWSP.op_Implicit W13
        | Register.R15D -> Arm64RegisterWOrWSP.op_Implicit W14
        | Register.ESP -> Arm64RegisterWOrWSP.op_Implicit WSP
        | otherwise -> invalidArg "register" $"{otherwise} is not a 32 bit register"

    let reg32 (register: Register) : Arm64RegisterW =
        match register with
        | Register.EDI -> W0
        | Register.ESI -> W1
        | Register.EDX -> W2
        | Register.R10D -> W3
        | Register.R8D -> W4
        | Register.R9D -> W5
        | Register.EBX -> W6
        | Register.ECX -> W7
        | Register.EAX -> W8
        | Register.EBP -> W9
        | Register.R11D -> W10
        | Register.R12D -> W11
        | Register.R13D -> W12
        | Register.R14D -> W13
        | Register.R15D -> W14
        | otherwise -> invalidArg "register" $"{otherwise} is not a 32 bit register"
