namespace aftermath

open Iced.Intel
open AsmArm64
open type AsmArm64.Arm64Factory

open aftermath.registers

module x86toarm =
    let private translate_instr (b: Arm64Assembler) (instr: Instruction) =
        match instr.Code with
        | Code.Mov_r32_imm32 ->
            let reg = arm.reg32 instr.Op0Register
            let immediate = instr.Immediate32
            b.MOVZ(reg, uint16 (immediate &&& 0xFFFFu))
            let x = uint16 (immediate >>> 16 &&& 0xFFFFu)

            if x <> 0us then // elide unnecessary instruction
                b.MOVK(reg, x, _LSL, 16)
        | Code.Xor_rm64_r64 ->
            b.EOR(arm.reg64 instr.Op0Register, arm.reg64 instr.Op0Register, arm.reg64 instr.Op1Register)
        | Code.Syscall ->
            b.MOV(X15, X0)
            b.SVC 0us
            b.MOV(X8, X0)
            b.MOV(X0, X15)
        | otherwise -> printfn $"Unsupported Instruction: {otherwise}"

    let public translate (source: byte array) =
        let reader = ByteArrayCodeReader source
        let decoder = Decoder.Create(64, reader)
        let mutable instr = Instruction()

        let buffer = Arm64InstructionBufferByList()
        let assembler = Arm64Assembler buffer

        while decoder.IP < uint64 source.Length do
            decoder.Decode(&instr)
            translate_instr assembler instr

        assembler.Assemble()

        for i in buffer.Instructions do
            printfn $"{Arm64Instruction.Decode(i)}"

        buffer
